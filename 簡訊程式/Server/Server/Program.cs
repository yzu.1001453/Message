﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace Time_Server
{
    class Program
    {
        private static byte[] _buffer = new byte[1024];
        private static List<Socket> _clientSockets = new List<Socket>();
        private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static Socket _clientSocket;


        public static void Main(string[] args)
        {
            Console.Title = "Server";            
            SetupServer();
            Console.ReadLine();
        }
        //Server啟動,等待Client連接
        private static void SetupServer()
        {
            try
            {
                Console.WriteLine("Waiting for a connection...");
                _serverSocket.Bind(new IPEndPoint(IPAddress.Any, 100));
                _serverSocket.Listen(10);
                //保持Server不斷線
                _serverSocket.IOControl(IOControlCode.KeepAliveValues, KeepAlive(1, 1000, 1000), null);
                //接收來自Client端的訊息
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (SocketException)
            {
                Console.WriteLine("Client failed!!");
            }
        }
        //Server端保持連線的函數
        private static byte[] KeepAlive(int onOff, int keepAliveTime, int keepAliveInterval)
        {
            byte[] buffer = new byte[12];
            BitConverter.GetBytes(onOff).CopyTo(buffer, 0);
            BitConverter.GetBytes(keepAliveTime).CopyTo(buffer, 4);
            BitConverter.GetBytes(keepAliveInterval).CopyTo(buffer, 8);
            return buffer;
        }
        //與Client端連接
        public static void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _serverSocket.EndAccept(AR);
            _clientSocket = socket;
            //取得來自Client端的IP位置
            IPEndPoint clientip = (IPEndPoint)_clientSocket.RemoteEndPoint;
            _clientSockets.Add(socket);
            Console.WriteLine("Client Connected..." + "IP address: " + clientip);
            FileStream fileappend = File.Open("log.txt", FileMode.Append);
            StreamWriter writer = new StreamWriter(fileappend);
            writer.Write(clientip + "  ");
            writer.Close();
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }
        //Server端接收到來自Client端的訊息,做適當的輸出與回覆
        private static void ReceiveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            if (socket.Connected == true)
            {
                try
                {
                    int received = socket.EndReceive(AR);
                    if (received > 0)
                    {
                        //接收到訊息,做日期輸出及訊息內容格式輸出
                        byte[] dateBuf = new byte[received];
                        Array.Copy(_buffer, dateBuf, received);
                        string text = Encoding.UTF8.GetString(dateBuf);//編碼
                        Console.WriteLine("Text received: " + text);
                        Console.WriteLine("Received time: " + DateTime.Now.ToString("yyyy-MM-dd(ddd) tt hh:mm:ss"));
                        FileStream fileappend = File.Open("log.txt", FileMode.Append);
                        StreamWriter writer = new StreamWriter(fileappend);
                        writer.Write(text + "  ");
                        writer.WriteLine(DateTime.Now + "  ");
                        writer.Close();
                        ProString(text);

                        //回覆Client端,已接收到訊息
                        string response = string.Empty;
                        response = text + " " + DateTime.Now.ToString("yyyy-MM-dd(ddd) tt hh:mm:ss");

                        byte[] data = Encoding.UTF8.GetBytes(response);//編碼
                        socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
                        socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
                    }
                    else
                    {
                        ;
                    }
                }
                catch (SocketException)
                {
                    Console.WriteLine("Client Unconnected");
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
            }
            else
            {
                socket.Close();
            }
        }

        private static void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
        //將指令及訊息內容加上格式輸出至SMSSender.exe
        private static void ProString(string text)
        {
            //開啟xml檔
            XmlTextReader reader = new XmlTextReader("Contact.xml");

            if (text[0] == '0' && text[1] == '9')
            {
                RunCmd(" /i /p:" + text.Substring(0, 10) + " /u /m:" + "\"" + text.Substring(11) + "\"");

            }
            else
            {
                while (reader.Read())//讀取xml檔的內容
                {

                    if (reader.NodeType == XmlNodeType.Element && reader.Name == text.Substring(0, 3))
                    {
                        while (reader.Read())
                        {
                            if (reader.Name == "member")
                                break;
                        }
                        for (; reader.Name == "member"; )
                        {
                            reader.Read();
                            RunCmd(" /i /p:" + reader.Value + " /u /m:" + "\"" + text.Substring(4) + "\"");
                            reader.Read();


                            while (reader.Read())
                            {
                                if (reader.Name == "member" || reader.Name == text.Substring(0, 3))
                                    break;
                            }
                        }
                        return;
                    }
                }
            }
        }
        //開啟cmd並執行SMSSender.exe
        static private string RunCmd(string command)
        {
            Process p = new Process();
            Console.WriteLine(command);
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.Arguments = "/c SMSSender.exe" + command;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;
            p.Start();

            p.StartInfo.Arguments = "/c exit";
            return p.StandardOutput.ReadToEnd();
        }
    }
}
